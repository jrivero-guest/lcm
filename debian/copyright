Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: lcm
Upstream-Contact: lcm-users@googlegroups.com
Source: https://github.com/lcm-proj/lcm
Files-Excluded: .github
 lcm-java/jchart2d-code
 test/gtest
 test/java/hamcrest-core-*.jar
 test/java/junit-*.jar
 WinSpecific

Files: *
Copyright: 2007-2024 LCM developers (see AUTHORS)
License: LGPL-2.1+

Files: lcm-lite/lcmlite_ios.*
Copyright: 2010 Edwin Olson
License: LGPL-2.1+

Files: cmake/3.7/UseJava.cmake
       cmake/3.7/UseJavaClassFilelist.cmake
       cmake/3.7/UseJavaSymlinks.cmake
Copyright: 2010-2011, Andreas schneider <asn@redhat.com>
           2010-2013, Kitware, Inc
           2013, OpenGamma Ltd <graham@opengamma.com>
License: BSD-3-clause


License: LGPL-2.1+
 This program is free software; you can redistribute it and/or modify it under
 the terms of the GNU Lesser General Public License as published by the Free
 Software Foundation; either version 2.1, or (at your option) any later version.
 .
 On Debian GNU/Linux systems, the complete text of the Lesser General Public
 License can be found in `/usr/share/common-licenses/LGPL-2.1'.

License: BSD-3-clause
 Software License Agreement (BSD License)
 .
 Copyright (c) 2008, Willow Garage, Inc.
 All rights reserved.
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
  * Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
  * Redistributions in binary form must reproduce the above
    copyright notice, this list of conditions and the following
    disclaimer in the documentation and/or other materials provided
    with the distribution.
  * Neither the name of the Willow Garage nor the names of its
    contributors may be used to endorse or promote products derived
    from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
